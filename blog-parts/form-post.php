

<?php
    $editing = isset($data);
    $url = $editing ? 'edit.php' : 'cria-post.php';
?>

<div class="row">
    <div class="col-md-6 mx-auto">
        <form class="text-center border border-light p-5" action="<?=$url?>" method="POST">

            <input value="<?= $editing ? $data[0] : ''?>" type="text" id="imagem" name="imagem" class="form-control mb-4" placeholder="URL da imagem">
            <input value="<?= $editing ? $data[1] : ''?>" type="text" id="cor_categoria" name="cor_categoria" class="form-control mb-4" placeholder="Cor da categoria">
            <input value="<?= $editing ? $data[2] : ''?>" type="text" id="categoria" name="categoria" class="form-control mb-4" placeholder="Categoria">
            <input value="<?= $editing ? $data[3] : ''?>" type="text" id="titulo" name="titulo" class="form-control mb-4" placeholder="Titulo da publicação">
            <input value="<?= $editing ? $data[4] : ''?>" type="text" id="conteudo" name="conteudo" class="form-control mb-4" placeholder="Conteudo">
            <input value="<?= $editing ? $data[5] : ''?>" type="text" id="autor" name="autor" class="form-control mb-4" placeholder="Nome do autor">
            
            <?php if(! $editing): ?>
                <input type="text" id="data" name="data" class="form-control mb-4" placeholder="Mês/Dia/Ano">
            <?php endif ?>

            <?php if($editing): ?>
                <input type="hidden" name="id" value="<? $id ?>" >
            <?php endif ?>

            <button class="btn btn-info btn-block my-4" type="submit"><?= $editing ? 'Editar' : 'Criar'?></button>


        </form>
    </div>
</div>