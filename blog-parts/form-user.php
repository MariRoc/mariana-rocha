

<?php
    $editing = isset($data);
    $url = $editing ? 'edit-user.php' : 'cria-user.php';
?>

<div class="row">
    <div class="col-md-6 mx-auto">
        <form class="text-center border border-light p-5" action="<?=$url?>" method="POST">

            <input value="<?= $editing ? $data[0] : ''?>" type="text" id="nome" name="nome" class="form-control mb-4" placeholder="Nome">
            <input value="<?= $editing ? $data[1] : ''?>" type="text" id="pais" name="pais" class="form-control mb-4" placeholder="País de residência">
            <input value="<?= $editing ? $data[2] : ''?>" type="text" id="idade" name="idade" class="form-control mb-4" placeholder="Idade">
            <input value="<?= $editing ? $data[3] : ''?>" type="text" id="profissao" name="profissao" class="form-control mb-4" placeholder="Profissão">
            
            <?php if($editing): ?>
                <input type="hidden" name="id" value="<? $id ?>" >
            <?php endif ?>

            <button class="btn btn-info btn-block my-4" type="submit"><?= $editing ? 'Editar' : 'Criar'?></button>


        </form>
    </div>
</div>