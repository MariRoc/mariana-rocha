<div class="container">
    <div class="card">
        <h3 class="card-header text-center font-weight-bold text-uppercase py-4">
            Área de edição de Usuário
            <a href= "http://localhost/marianarocha/mariana-rocha/new-user.php?id='.$id.'"><i class="fas fa-plus green-text"></i></a>
        </h3>
        <div class="card-body">
            <div id="table" class="table-editable">
                <span class="table-add float-right mb-3 mr-2">
                    <a href="#!" class="text-success"></a>
                </span>
                <table class=" table-responsive-md table-striped text-center">
                    <thead >
                        <tr>
                            <th class="text-center">Nome</th>
                            <th> </th>
                            <th class="text-center">País</th>
                            <th> </th>
                            <th class="text-center">Idade</th>
                            <th> </th>
                            <th class="text-center">Profissão</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?= $table?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>