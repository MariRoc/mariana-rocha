<?php
    function introSection () { 
        return '
            <div class="view jarallax" data-jarallax=\'{"speed": 0.2}\' style="background-image: url(https://cdn.pixabay.com/photo/2018/02/12/09/39/background-3147808_960_720.jpg); background-repeat: no-repeat; background-size: cover; background-position: center center;">
                <div class="mask rgba-black-light">
                    <div class="container h-100 d-flex justify-content-center align-items-center">
                        <div class="row pt-5 mt-3">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <h1 class="h1-reponsive white-text text-uppercase font-weight-bold mb-3 wow fadeInDown" data-wow-delay="0.3s"><strong>Seja bem-vindo ao meu blog!</strong></h1>
                                    <hr class="hr-light mt-4 wow fadeInDown" data-wow-delay="0.4s">
                                    <h5 class="text-uppercase mb-5 white-text wow fadeInDown" data-wow-delay="0.4s"><strong>Criação e Redação de Mariana Rocha - GU3015173</strong></h5>
                                    <a href= "http://localhost/marianarocha/mariana-rocha/lista-post.php"class="btn btn-outline-white wow fadeInDown" data-wow-delay="0.4s">Todos os posts</a>
                                    <a href= "http://localhost/marianarocha/mariana-rocha/lista-user.php"class="btn btn-outline-white wow fadeInDown" data-wow-delay="0.4s">Usuários</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        ';
    }
    function postRow($row){
        $html = '
            <div class="row">

                    <div class="col-lg-4 mb-4">
                    
                        <div class="view overlay z-depth-1">
                            <img src="'. $row[0] .'" class="img-fluid" alt="First sample image">
                            <a>
                            <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                    </div>
                
                
                
                    <div class="col-lg-7 mb-4">
                    
                        <a href="" class="' . $row[1] .'">
                            <h6 class="pb-1"><i class="fas fa-heart"></i><strong> '. $row[2] .' </strong></h6>
                        </a>
                    
                        <h4 class="mb-4"><strong>'. $row[3].'</strong></h4>
                        <p>'. $row[4] .'</p>
                        <p>by <a><strong> '. $row[5] .'</strong></a> '. $row[6] .'</p>
                        <a class="btn btn-primary">Ler post completo</a>
                
                    </div>
      
      
            </div>
      
            <hr class="mb-5">';

        return $html;
    }

    function getsRecentPost(array $m){
        $recentPost = '';
        foreach ($m as $row){
            $recentPost .= postRow($row);
        }
        return $recentPost;
    }

    include_once 'db-manager.php';

    function getPostTable(){
        $html = '';

        $m = getPosts();

        foreach ($m as $row) {
            
            $html .= '<tr>';
            $html .= '<th class="pt-3-half" >'.$row[0].'<th>';
            $html .= '<th class="pt-3-half" >'.$row[1].'<th>';
            $html .= '<th class="pt-3-half" >'.$row[2].'<th>';
            $html .= '<th class="pt-3-half" >'.$row[3].'<th>';
            $html .= '<th class="pt-3-half" >'.$row[4].'<th>';
            $html .= '<th class="pt-3-half" >'.$row[5].'<th>';
            $html .= '<th class="pt-3-half" >'.createLink($row[7]).'<th>';
          
            $html .= '<tr>';
        }
        return $html;
    }

    function createLink($id){
        $html = '<a href= "http://localhost/marianarocha/mariana-rocha/edit.php?id='.$id.'"> <i class="far fa-edit blue-text"></i></a>';
        $html .= '<a href= "http://localhost/marianarocha/mariana-rocha/delete.php?id='.$id.'"> <i class="far fa-trash-alt red-text"></i></a>';
        
        return $html;
    }
    
    include_once 'db-manager.php';

    function getUserTable(){
        $html = '';

        $muser = getUser();

        foreach ($muser as $row) {
            
            $html .= '<tr>';
            $html .= '<th class="pt-3-half" >'.$row[0].'<th>';
            $html .= '<th class="pt-3-half" >'.$row[1].'<th>';
            $html .= '<th class="pt-3-half" >'.$row[2].'<th>';
            $html .= '<th class="pt-3-half" >'.$row[3].'<th>';
            $html .= '<th class="pt-3-half" >'.createLinkUser($row[4]).'<th>';
          
            $html .= '<tr>';
        }
        return $html;
    }
    function createLinkUser($id){
        $html = '<a href= "http://localhost/marianarocha/mariana-rocha/edit-user.php?id='.$id.'"> <i class="far fa-edit blue-text"></i></a>';
        $html .= '<a href= "http://localhost/marianarocha/mariana-rocha/delete-user.php?id='.$id.'"> <i class="far fa-trash-alt red-text"></i></a>';
        
        return $html;
    }

    $mOlder = [
        [
            'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(131).jpg',
            'pink-text',
            'Adventure',
            'This is title of the news',
            'Billy Forester',
            '15/07/2016',
            'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime
            placeat facere possimus voluptas.'
        ],
    
        [
            'https://mdbootstrap.com/img/Photos/Others/img6.jpg',
            'indigo-text',
            'Education',
            'This is title of the news',
            'Billy Forester',
            '12/07/2016',
            'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum
            deleniti atque corrupti quos dolores.'
        ],
    
        [
            'https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(33).jpg',
            'cyan-text',
            'Culture',
            'This is title of the news',
            'Billy Forester',
            '10/07/2016',
            'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, quia consequuntur magni
            dolores eos qui ratione voluptatem.'
        ],
    ];

    $olderPost = '';
    foreach ($mOlder as $row){
        $olderPost .= postRow($row);
    }
?>