
<?php include 'config/cabeçalho.php' ?>

<?php 
     include 'blog-parts/model/data-model.php';
     include 'blog-parts/model/blog-model.php';
     $introduction = introSection ();
     include 'util/header.php';
?>

<?php include 'blog-parts/view/main.php' ?>

<?php include 'util/footer.php' ?>

<?php include 'config/rodape.php'?>
