<?php include 'config/cabeçalho.php' ?>

<div class="row">
    <div class="col-md-6 mx-auto">
        <form class="text-center border border-light p-5" >
            <h1>Edite as informações desejadas</h1>
        </form>
    </div>
</div>

<?php 
    include_once 'blog-parts/model/db-manager.php';
    $id = $_GET['id'];
    $data = getEditUser($id);
?>

<?php include 'blog-parts/form-user.php' ?>
<?php include 'util/footer.php' ?>
<?php include 'config/rodape.php'?>